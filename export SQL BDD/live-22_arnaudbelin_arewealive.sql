-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mar. 11 fév. 2020 à 16:27
-- Version du serveur :  5.7.29-0ubuntu0.18.04.1
-- Version de PHP :  7.3.6-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `live-22_arnaudbelin_arewealive`
--

-- --------------------------------------------------------

--
-- Structure de la table `booked`
--

CREATE TABLE `booked` (
  `booked_id` int(11) NOT NULL,
  `concert_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `book_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `booked`
--

INSERT INTO `booked` (`booked_id`, `concert_id`, `user_id`, `user_email`, `book_number`) VALUES
(61, 9, 5, 'dorian@gmail.com', 5),
(62, 13, 5, 'dorian@gmail.com', 4),
(63, 14, 5, 'dorian@gmail.com', 1),
(64, 10, 20, 'koeb@gmail.com', 3),
(65, 9, 21, 'eddie@clarck.com', 1),
(66, 10, 21, 'eddie@clarck.com', 1),
(67, 14, 21, 'eddie@clarck.com', 1),
(68, 13, 21, 'eddie@clarck.com', 1),
(69, 8, 21, 'eddie@clarck.com', 1),
(70, 8, 22, 'celinekoeberle@gmail.com', 4),
(71, 9, 23, 'clement.toledano@gmail.com', 3);

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date_comment` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `author`, `content`, `date_comment`) VALUES
(147, 'igor', 'Pariatur ipsum eum quia expedita amet. Facere dolores voluptates ea odit quia possimus rem. Iste in commodi dolorum est id. Sed doloribus voluptate rem sed qui dolorum.', '2020-02-01 15:19:24'),
(148, 'max', 'Quis non et voluptatibus aut quo. Libero aut eveniet ipsa quo vel sint minima. Quidem in occaecati nemo doloribus. Ad nemo odio aut blanditiis laboriosam aut iste.', '2020-02-01 15:19:28'),
(149, 'bruce', 'Et ut autem quibusdam saepe labore animi aut. Tempora esse ab rerum repudiandae culpa molestias. Omnis sit quas voluptas cum illum voluptatum velit accusamus. Ullam minima dolor qui quas.', '2020-02-01 15:19:31'),
(150, 'steve', 'Quo eum illo sint et. Aliquid atque sunt labore eum fuga perferendis. Quas temporibus sequi hic consequatur iste voluptatibus. Iure ullam voluptas molestiae dolores facere sit.', '2020-02-01 15:19:34'),
(151, 'lars', 'Harum aut laboriosam ea vel voluptatum adipisci nihil. Tenetur doloribus molestiae iste quo. Dolorem placeat molestiae et dolore. Qui ex cumque sit vitae voluptate qui. Eius est amet quisquam accusantium quos.', '2020-02-01 15:19:37'),
(152, 'tom', 'Tempora placeat ipsa ut laboriosam in. Quo doloribus aut ea eum. Maxime consectetur qui nihil expedita ut ut.', '2020-02-01 15:20:12'),
(153, 'kerry', 'Et perferendis et rerum dolorem voluptatum aut. Repellendus ad omnis aut odio doloribus eum. Voluptas ullam recusandae fugiat fugiat molestiae.', '2020-02-01 15:20:15'),
(154, 'mike', 'Occaecati est error exercitationem fuga. Omnis aut omnis et magni quod. Atque asperiores molestias rerum qui. Adipisci iste atque hic architecto.', '2020-02-01 15:20:18'),
(155, 'jeff', 'Illum ab aut quo sed quia. Debitis voluptatibus ut praesentium est et est consequuntur. Reprehenderit sunt quas nesciunt. Sint aut esse ut sunt. Voluptas possimus in perspiciatis sed blanditiis est culpa.', '2020-02-01 15:20:21'),
(156, 'rob', 'Tempore accusantium error libero commodi corporis. Occaecati numquam amet laboriosam iure maxime. Sint est necessitatibus ad neque.', '2020-02-01 15:20:24'),
(157, 'ozzy', 'Qui aperiam ut deserunt repudiandae aut sequi quod. Pariatur eum enim non velit. Velit alias mollitia in quis quo.', '2020-02-01 15:20:28'),
(158, 'lee', 'Quae possimus consequatur et et totam quia minima. Inventore magnam et optio esse ut quas. Et omnis illo consectetur accusantium dicta voluptatem. Et repudiandae cum aliquam deleniti tempora dolores.', '2020-02-01 15:20:31'),
(159, 'jon', 'Qui ea et amet sed officiis rem. Qui libero sit mollitia autem. Beatae consequuntur ut qui rerum quibusdam nulla. Vitae adipisci aut sint est sit perferendis. Minus eaque aliquam cum voluptatum quae libero.', '2020-02-01 15:20:33'),
(167, 'Bruce', '#### off !! Its just a big #### !', '2020-02-09 13:22:09'),
(201, 'Eddie', 'Hail guys!! Your show in Noisy le Sec was awesome!! Thanks', '2020-02-10 14:15:02'),
(202, 'Mentoss', 'Meilleur album au monde !', '2020-02-10 17:05:56');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id_message` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `done` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id_message`, `user_id`, `title`, `description`, `done`) VALUES
(16, 5, 'test1', 'test1', 0),
(17, 5, 'test2', 'test2', 1),
(18, 5, 'test3', 'test3', 1),
(19, 5, '&lt;script&gt;alert&#40;\'hello\'&#41;;&lt;/script&gt;', 'script&gt;alert&#40;\'hello\'&#41;;&lt;/script&gt;', 1),
(20, 5, '&lt;strong&gt;test&lt;/strong&gt;', '&lt;strong&gt;test&lt;/strong&gt;', NULL),
(21, 5, 'fffffff', 'ddddddd', 1),
(22, 5, '5555', 'sdfgh', 1),
(23, 5, '&lt;strong&gt;test&lt;/strong&gt;', '&lt;script&gt;alert&#40;&quot;ta mere&quot;&#41;&lt;/script&gt;', 1),
(25, 5, 'gg', 'gg', 1),
(26, 5, 'gg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet', NULL),
(27, 5, 'test', 'test', 1),
(28, 20, 'ta mere', 'tata', 1),
(29, 21, 'dd', 'dd', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title_news` varchar(255) NOT NULL,
  `body_news` varchar(500) NOT NULL,
  `date_news` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `news`
--

INSERT INTO `news` (`id`, `title_news`, `body_news`, `date_news`) VALUES
(13, 'Studio session', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.', '2020-01-08'),
(14, 'New Song !!', 'There are many variations of passages of Lorem Ipsum available,', '2020-02-02'),
(15, 'European tour', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet', '2020-02-08'),
(19, 'Punk ipsum !!', 'Cupcake ipsum dolor sit amet tiramisu bonbon jujubes brownie. Halvah topping lemon drops. Croissant dessert dragée. Jelly candy pie danish jelly lemon drops croissant tiramisu cake. Chocolate cake chocolate cake biscuit cake gummies. Apple pie wafer jelly beans jelly jelly candy canes.\r\n\r\nGummi bears pudding tiramisu powder marzipan. Soufflé gingerbread soufflé muffin chocolate. Toffee oat cake fruitcake jujubes jelly beans cupcake jelly-o. Gummi bears tart caramels icing croissant. Toffee pastr', '2020-02-10');

-- --------------------------------------------------------

--
-- Structure de la table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `email_newsletter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `newsletter`
--

INSERT INTO `newsletter` (`id`, `email_newsletter`) VALUES
(8, 'aa@aa.com'),
(9, 'bb@bb.com'),
(7, 'celinekoeberle@gmail.com'),
(6, 'eddie@clarck.com'),
(2, 'ghost@gmail.com'),
(5, 'koeb@gmail.com'),
(4, 'steer@gmail.com'),
(1, 'test@test.com'),
(3, 'trudeau@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `rating_album`
--

CREATE TABLE `rating_album` (
  `id_rate` int(11) NOT NULL,
  `rate` int(11) DEFAULT NULL,
  `date_rate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_adress` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rating_album`
--

INSERT INTO `rating_album` (`id_rate`, `rate`, `date_rate`, `ip_adress`) VALUES
(428, 5, '2020-02-03 19:10:54', '::1'),
(429, 3, '2020-02-08 17:28:30', '::2'),
(434, 4, '2020-02-10 15:45:43', '127.0.0.1');

-- --------------------------------------------------------

--
-- Structure de la table `tour`
--

CREATE TABLE `tour` (
  `id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `concert_hall` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `book` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tour`
--

INSERT INTO `tour` (`id`, `city`, `country`, `concert_hall`, `date`, `book`) VALUES
(8, 'berlin', 'germany', 'huxley', '2020-04-09', 15),
(9, 'london', 'england', 'underworld', '2020-03-18', 19),
(10, 'barcelona', 'spain', 'razzmatazz', '2020-03-20', 14),
(11, 'paris', 'france', 'bataclan', '2020-02-05', 1),
(13, 'Gothenburg', 'Sweden', 'Pustervik', '2020-06-20', 5),
(18, 'montreal', 'canada', 'Katacomb', '2020-07-04', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `choose_newsletter` int(11) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `choose_newsletter`, `admin`) VALUES
(1, 'igor', 'cavalera', 'cavalera@gmail.com', '$2y$10$LLJDzYJ/21c15jZA8H4PC.yJftZFya.FOqHxpwHLxPRc74Bvzz./i', 1, NULL),
(3, 'james', 'hetfield', 'metallica@gmail.com', '$2y$10$K1L2xxOsej7ncgSyDeu0bOt3oJ4qVB0wbQP1ObrRlcJeEWYJvILyO', 1, NULL),
(4, 'steve', 'harris', 'steve@gmail.com', '$2y$10$Lis78rdZrl5IQ6TKcSDWuOLVBBhvdHxN99TFBiNdS9WngVGBUUJWC', NULL, NULL),
(5, 'lee', 'dorian', 'dorian@gmail.com', '$2y$10$MaCer/mcdxYhLNk2/Q72cuqvzU8xuteZJSsqYHukG2Gm/H.JGzzMu', NULL, 1),
(8, 'papa', 'ghost', 'ghost@gmail.com', '$2y$10$qpzY.IkNI6PA0m23QE9H1OP0VtS8URN2rYdOVnrikfn9jdKq0VAFa', 1, NULL),
(11, 'justin', 'trudeau', 'trudeau@gmail.com', '$2y$10$acTutRsjclGshUJnzQMcCuHM2dv69Sq0owdXsuVuqWxbKnOQuopsC', 1, NULL),
(17, 'nasty', 'nasty', 'nasty@gmail.com', '$2y$10$fOmnKthyCi5ivJgwPKeCeeyN7oYz6T9V9phMTRjDmucmKiCnVxgFe', 0, NULL),
(18, 'bill', 'steer', 'steer@gmail.com', '$2y$10$/Io6trGrrOT3AXTmVRtFeeJDKgLYj9jRL.uVisaBHcT.3UrFozEA.', 0, NULL),
(19, 'tom', 'araya', 'araya@gmail.com', '$2y$10$zz3YRTa2WSglQ8yxwBSJ0e1qQ4uIUkSuOfSrqyNQ7dzEfCfXWhuUe', 0, NULL),
(20, 'celine', 'koeb', 'koeb@gmail.com', '$2y$10$AOi7pBUxXVW2I00M3o9nSuNmAFKlk/9Oe6RkEThaSjtqS1i8D9qUu', 1, NULL),
(21, 'eddie', 'clark', 'eddie@clarck.com', '$2y$10$oGp7tTpV9klr9qUG04/W3.ueY.w5.OJ7q8Bsa6zWA.Bvl0OROY7Kq', 1, 1),
(22, 'Céline', 'KOEBERLE', 'celinekoeberle@gmail.com', '$2y$10$VQkgBT61rqNierQVlC72deKWD3/e5vKdUAeOEtMNu0INdKne4U4LG', 1, NULL),
(23, 'Clément', 'TOLEDANO', 'clement.toledano@gmail.com', '$2y$10$hnocE9tb86YFjZzkbJj3.uhFbQkg/nPzGYpMJoeeoxOD5ZshboktW', 0, 1),
(24, 'papa', 'papa', 'papa@ghost.com', '$2y$10$1bqBYgBy5xSx4OkIzV6MQecC6tPddO2i7ZrC6uRTffHTmL2lrz9B.', 0, NULL),
(25, 'admin', 'admin', 'admin@admin.com', '$2y$10$Yj20f6MTXIiUyb63cRO02.LgYyqdV0ESYCzhVgdpygL6cuXkEDglK', 0, 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `booked`
--
ALTER TABLE `booked`
  ADD PRIMARY KEY (`booked_id`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id_message`);

--
-- Index pour la table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_newsletter` (`email_newsletter`);

--
-- Index pour la table `rating_album`
--
ALTER TABLE `rating_album`
  ADD PRIMARY KEY (`id_rate`);

--
-- Index pour la table `tour`
--
ALTER TABLE `tour`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `booked`
--
ALTER TABLE `booked`
  MODIFY `booked_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id_message` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT pour la table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `rating_album`
--
ALTER TABLE `rating_album`
  MODIFY `id_rate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=435;

--
-- AUTO_INCREMENT pour la table `tour`
--
ALTER TABLE `tour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
