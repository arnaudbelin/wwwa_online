<?php 

class Users extends CI_Controller {

	public function logout(){

		$this->session->sess_destroy();

		redirect('home/index');

	}

	public function login(){

		$this->form_validation->set_rules('email', 'Email', 'trim|htmlspecialchars|required|valid_email|min_length[3]');
		$this->form_validation->set_rules('password', 'Password', 'trim|htmlspecialchars|required|min_length[3]');

		if($this->form_validation->run() == FALSE){

			$this->session->set_flashdata('login_fail', 'Sorry, your login has failed');

			redirect('home');
		}else{
			
			$email = $this->security->xss_clean($this->input->post('email'));

			$password = $this->security->xss_clean($this->input->post('password'));

			$result = $this->user_model->login_user($email, $password);

			if($result) {

				$user_data = array(

					'user_id' => $result->id,
					'first_name' =>$result->first_name,
					'last_name' =>$result->last_name,
					'email' =>$result->email,
					'logged_in' => true,
					'logged_in_admin' => $result->admin

				);

				$this->session->set_userdata($user_data);

				redirect('home/index');

			} else{

				$this->session->set_flashdata('login_fail', 'Sorry, your login has failed');

				redirect('home/index');

			}

		}
	}

	public function register(){

		$this->form_validation->set_rules('first_name', 'First Name', 'trim|htmlspecialchars|required|min_length[2]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|htmlspecialchars|required|min_length[2]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|htmlspecialchars|min_length[3]|valid_email');
		$this->form_validation->set_rules('password', 'Password ', 'trim|required|regex_match[#^(?=.*[a-z])(?=.*[0-9])#]|htmlspecialchars|min_length[8]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|htmlspecialchars|required|matches[password]');

		if($this->form_validation->run() == FALSE){

			$data['main_view'] = 'register_view';

			$this->load->view('layouts/main', $data);

		}else{

			$encripted_pass = password_hash($this->input->post('password'), PASSWORD_BCRYPT);

			$data = array (

				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'email' => $this->input->post('email'),
				'password' => $encripted_pass, 
				'choose_newsletter' => $this->input->post('choose_newsletter')

			);

			$data = $this->security->xss_clean($data);

			$result = $this->user_model->create_user($data);

			if($result){

				$this->session->set_flashdata('user_registered', 'User has been register, you can log now');

				redirect('home/index');

			} else{

				$this->session->set_flashdata('user_registered_prob', 'User has not been register, please retry');

				redirect('home/index');

			}

		}

	}

}

?>
