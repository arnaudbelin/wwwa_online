<div id="update_date" class="container">
    <?php $attributes = array('class'=>'form') ?>
    <?php echo validation_errors("<p class = 'alert alert-danger'/p>"); ?>
    <?php echo form_open("admin/create_news", $attributes); ?>
    <div class="form-group">
      <?php echo form_label('Title'); ?>
      <?php 
      $data = array(
        'class' => 'form-control',
        'name' => 'title_news'
    );
    ?>
    <?php echo form_input($data); ?>
    </div>
        <div class="form-group">
      <?php echo form_label('Body'); ?>
      <?php 
      $data = array(
        'class' => 'form-control',
        'name' => 'body_news'
    );
    ?>
    <?php echo form_textarea($data); ?>
    </div>
    <div class="form-group">
      <?php 
      $data = array(
        'class' => 'btn bouton',
        'value' => 'CREATE'
    );
    ?>
    <?php echo form_submit($data); ?>
    </div>
    <?php echo form_close(); ?>
</div>