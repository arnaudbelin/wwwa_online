<div class="container col-12 d-flex justify-content-around" id="registerback" >
 <div id="register"  >
   <div class="row">
     <div id="concert_book"  class="text-dark col-lg-8">
       <h2 class="text-light text-capitalize">Hello <?php echo $this->session->userdata('first_name'); ?></h2>	
       <h1 class="text-light">Book your Tickets</h1>
       <form method="post" TARGET=_BLANK action="<?php echo base_url(); ?>home/book_ticket/<?php echo $concert->id ?>">
         <div class="form-group">  
          <input type="text" class="form-control text-capitalize" name="city" value="<?php echo $concert->city; ?>" disabled>  
      </div>
      <div class="form-group">   
          <input type="text" class="form-control text-uppercase" name="country" value="<?php echo $concert->country; ?>" disabled>
      </div>
      <div class="form-group">   
          <input type="text" class="form-control text-capitalize" name="concert_hall" value="<?php echo $concert->concert_hall; ?>" disabled>
      </div>
      <div class="form-group">   
          <input type="text" class="form-control" name="date" value="<?php echo $concert->date; ?>" disabled>
      </div>
      <div class="form-group">
          <label class="text-light  h6" for="book_number">Number of tickets</label>
          <select class="form-control form-control-sm" name="book_number">
           <option>1</option>
           <option>2</option>
           <option>3</option>
           <option>4</option>
           <option>5</option>
       </select>
   </div>
   <button type="submit" class="bouton btn ">Submit</button>
</form>
</div>                   
</div>
</div>                    
</div>
