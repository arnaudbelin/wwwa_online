<!doctype html>
<html lang="en">
<head>
    <meta name="description" content="Official website of the Hardcore band : Are We Alive?" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="UTF-8">
    <title>wwwa</title>
    <link href="https://fonts.googleapis.com/css?family=Oswald|Special+Elite&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/4de7108ca3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href=" <?php echo base_url();?>/assets/css/normalize.css" />
    <link rel="stylesheet" href=" <?php echo base_url();?>/assets/css/style.css" /> 
</head>
<body>
    <header > 
    

        <?php 
        $tour = '';
        $album = '';
        $contact = '';
        $news = '';
        $admin = '';

        if($this->uri->segment(2) == 'tour_date'){
           $tour = "active";
       }else if($this->uri->segment(2) == 'album'){
        $album = "active";
    }else if($this->uri->segment(2) == 'contact'){
        $contact = "active";
    }else if($this->uri->segment(2) == 'news'){
        $news = "active";
    }else if($this->uri->segment(1) == 'admin'){
        $admin = "active";
    }

    ?>
    <!-- Modal Cookie-->
    <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-dark" >Cookies</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body text-dark">
        <p>To make this site work properly, we sometimes place small data files called cookies on your device. Most big websites do this too.</p>

        <h3>What are cookies?</h3>

        <p>A cookie is a small text file that a website saves on your computer or mobile device when you visit the site. It enables the website to remember your actions and preferences (such as login, language, font size and other display preferences) over a period of time, so you don’t have to keep re-entering them whenever you come back to the site or browse from one page to another.</p>

        <h3>How do we use cookies?</h3>

        <p>Adjust this part of the page according to your needs. Explain which cookies you usein plain, jargon-free language. In particular:</p><br>

        <p>their purpose and the reason why they are being used, (e.g. to remember users' actions, to identify the user, for online behavioural advertising)</p>
        <p>if they are essential for the website or a given functionality to work or if they aim to enhance the performance of the website</p>
        <p>the types of cookies used (e.g. session or permanent, first or third-party)
            who controls/accesses the cookie-related information (website or third party)
            that the cookie will not be used for any purpose other than the one stated
        how consent can be withdrawn.</p>

        <h3>How to control cookies</h3>

        <p>You can control and/or delete cookies as you wish. You can delete all cookies that are already on your computer and you can set most browsers to prevent them from being placed. If you do this, however, you may have to manually adjust some preferences every time you visit a site and some services and functionalities may not work.</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div>
<h1 class="logo"><a href="<?php echo base_url(); ?>home">ARE WE ALIVE?</a></h1>
<nav class="menu">
    <a class = "<?php echo $tour ?>" href="<?php echo base_url(); ?>home/tour_date#blur">TOUR DATE</a>
    <a class = "<?php echo $album ?>" href="<?php echo base_url(); ?>home/album#albumback">ALBUM</a>
    <a class = "<?php echo $news ?>" href="<?php echo base_url(); ?>home/news#news">NEWS</a> 
    <a class = "<?php echo $contact ?>" href="<?php echo base_url(); ?>home/contact#contact">CONTACT US</a>
    <?php if($this->session->userdata('logged_in_admin') == 1):?> 
        <a class = "<?php echo $admin ?>"  href="<?php echo base_url(); ?>admin">ADMIN</a>
    <?php endif; ?>
    <?php if($this->session->userdata('logged_in') == false):?> 
        <a id="log_nav" data-toggle="modal" data-target="#exampleModal" href="">LOGIN</a>
    <?php endif; ?>
    <?php if($this->session->userdata('logged_in')):?> 
        <a id="log_nav_out" href="<?php echo base_url(); ?>users/logout">LOGOUT</a>
    <?php endif; ?> 
</nav>
</header>
<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content bg-dark text-light">
      <div class="modal-header">
        <h5 class="modal-title">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
       </button>
   </div>
   <div class="modal-body">
    <?php $attributes = array('id' => 'login_form', 'class'=>'form_group') ?>
    <?php if($this->session->flashdata('errors')): ?>
        <?php echo $this->session->flashdata('errors') ?>
    <?php endif; ?>
    <?php echo form_open('users/login', $attributes); ?>
    <div class="form-group">

        <?php echo form_label('Email'); ?>
        <?php 
        $data = array(
            'class' => 'form-control',
            'name' => 'email',
            'placeholder' => 'Enter Email'
        );
        ?>
        <?php echo form_input($data); ?>
    </div>
    <div class="form-group">

        <?php echo form_label('Password'); ?>
        <?php 
        $data = array(
            'class' => 'form-control',
            'name' => 'password',
            'placeholder' => 'Enter Password'
        );
        ?>
        <?php echo form_password($data); ?>
    </div>

    <div class="form-group">
        <?php 
        $data = array(
            'class' => 'btn bouton',
            'name' => 'submit',
            'value' => 'Login'
        );
        ?>
        <?php echo form_submit($data); ?>
    </div>
    <?php echo form_close(); ?>
</div>
<div class="modal-footer">
    <a class="btn bouton" href="<?php echo base_url();?>users/register" >Register HERE</a>
</div>
</div>
</div>
</div>
<main>
    <?php $this->load->view($main_view); ?>
</main>
<footer>
    <div>
        <fieldset>
            <legend>JOIN OUR NEWSLETTER</legend>
            <form action="<?php base_url(); ?>home/newsletter" method="post">
                <input type="email" name="email_newsletter" placeholder="Your Email Adress" />
                <input type="submit" value="SIGN UP" />
            </form>
        </fieldset>
    </div>
    <div class="social">
        <nav>
            <a href="https://www.instagram.com/?hl=fr" target="_blank"><i class="fab fa-instagram"></i></a>
            <a href="https://fr-fr.facebook.com/" target="_blank"><i class="fab fa-facebook-square"></i></a>
            <a href="https://www.spotify.com/fr/" target="_blank"><i class="fab fa-spotify"></i></a>
        </nav>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/scriptcom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/rgpd.js"></script>
</body>
</html>
