<div class="container col-12 d-flex justify-content-around" id="registerback" >
	<div id="register"  >
		<h2>Register</h2>
		<?php $attributes = array('id' => 'register_form', 'class'=>'form') ?>
		<?php echo validation_errors("<p class = 'text-danger'/p>"); ?>
		<?php echo form_open('users/register', $attributes); ?>
		<div class="form-group">
			<?php echo form_label('First Name'); ?>
			<?php 
			$data = array(
				'class' => 'form-control',
				'name' => 'first_name',
				'placeholder' => 'Enter First Name',
				'value' => set_value('first_name')
			);
			?>
			<?php echo form_input($data); ?>
		</div>
		<div class="form-group">
			<?php echo form_label('Last Name'); ?>
			<?php 
			$data = array(
				'class' => 'form-control',
				'name' => 'last_name',
				'placeholder' => 'Enter Last Name',
				'value' => set_value('last_name')
			);
			?>
			<?php echo form_input($data); ?>
		</div>
		<div class="form-group">
			<?php echo form_label('Email'); ?>
			<?php 
			$data = array(
				'class' => 'form-control',
				'name' => 'email',
				'placeholder' => 'Enter Your Email',
				'value' => set_value('email')
			);
			?>
			<?php echo form_input($data); ?>
		</div>
		<div class="form-group">
			<?php echo form_label('Must be contain 8 characters min with alpha and numbers'); ?>
			<?php 
			$data = array(
				'class' => 'form-control',
				'name' => 'password',
				'placeholder' => 'Enter Your Password'
			);
			?>
			<?php echo form_password($data); ?>
		</div>
		<div class="form-group">
			<?php echo form_label('Confirm Password'); ?>
			<?php 
			$data = array(
				'class' => 'form-control',
				'name' => 'confirm_password',
				'placeholder' => 'Confirm Password'
			);
			?>
			<?php echo form_password($data); ?>
		</div>
		<div class="form-form">
			<?php 
			echo form_label('I suscribe to the Newsletter', 'choose_newsletter'); ?>
			<?php 
			$data = array(
				'class' => 'form-control',
				'name' => 'choose_newsletter',
				'value'         => '1'	
			);
			?>
			<?php echo form_checkbox($data); ?>
		</div>
		<div class="form-group">
			<?php 
			$data = array(
				'class' => 'btn bouton',
				'name' => 'submit',
				'value' => 'Register'
			);
			?>
			<?php echo form_submit($data); ?>
		</div>
	</div>
</div>
<?php echo form_close(); ?>




