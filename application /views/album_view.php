<div class=" col-12 d-flex justify-content-around" id="albumback" >
	<div class="row">
		<div id="album">
			<div id="cover_album"  class="text-dark col-lg-3">
				<figure>
					<a href="https://www.amazon.co.uk/" target ="blank"><img src="<?php echo base_url(); ?>/assets/image/cover1.jpg " alt=""></a>
				</figure>				
			</div>
			<div  id="mag_q" class="text-dark col-lg-6">
				<ul>
					<li><q>Amazing and catchy! </q><i> Kerrang Magazine</i></li>
					<li><q>The album of the year</q><i> Rolling Stones</i> </li>
					<li><q>So pure so MOSH</q><i> Decibel</i></li>
					<li><q>True Hardcore spirit</q><i> Vice</i></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div id="chat_album">
	<div class="container "  >
		<div id="com" class="row text-dark" >
			<div class="col-lg-4 pt-5 text-light ">
				<p>Rating score : <span id="avaragedb"><?php echo $average->rate ?></span></p>
				<div id="contain_rate">
					<div id="value">
						<div id="rate_star">
							<i id="tde_1" class="far fa-star tde"></i>
							<i id="tde_2" class="far fa-star tde"></i>
							<i id="tde_3" class="far fa-star tde"></i>
							<i id="tde_4" class="far fa-star tde"></i>
							<i id="tde_5" class="far fa-star tde"></i>
						</div>
					</div>
				</div>
				<h2>Please, rate and comment</h2>
				<?php $attributes = array('id' => 'comment_form', 'class'=>'form') ?>		
				<?php echo form_open('home/comment?task=write', $attributes); ?>
				<div class="form-group">
					<?php echo form_label('Author'); ?>
					<?php 
					$data = array(
						'class' => 'form-control',
						'id' => 'author',
						'name' => 'author'			
					);
					?>
					<?php echo form_input($data); ?>
				</div>
				<div class="form-group">
					<?php echo form_label('Comment'); ?>
					<?php 
					$data = array(
						'class' => 'form-control',
						'id' => 'content',
						'name' => 'content'
					);
					?>
					<?php echo form_textarea($data); ?>
				</div>
				<div class="form-group">
					<?php 
					$data = array(
						'class' => 'btn bouton',
						'name' => 'submit', 
						'value'=> 'SUBMIT'
					);
					?>
					<?php echo form_submit($data); ?>
				</div>
				<?php echo form_close(); ?>
			</div>
			<div id="display_comments" class=" col-lg-8 pt-3">
				<!-- fill dynamicly by js -->
			</div>
		</div>
	</div>
</div>











