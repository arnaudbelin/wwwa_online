<div id="home" class ="row m-0">
    <div class="mw-100 col-12 d-flex align-items-center justify-content-center">
      <p class="text-danger">
    <?php if($this->session->flashdata('no_access')): ?>
    <?php echo $this->session->flashdata('no_access')?>
<?php endif; ?></p>
 <p class="text-danger">
    <?php if($this->session->flashdata('already_booked')): ?>
    <?php echo $this->session->flashdata('already_booked')?>
<?php endif; ?></p>
<p class="text-danger">
    <?php if($this->session->flashdata('email_error')): ?>
    <?php echo $this->session->flashdata('email_error')?>
<?php endif; ?></p>
 <p class="text-danger">
    <?php if($this->session->flashdata('problem_booked')): ?>
    <?php echo $this->session->flashdata('problem_booked')?>
<?php endif; ?></p>
 <p class="text-success">
    <?php if($this->session->flashdata('contact_success')): ?>
    <?php echo $this->session->flashdata('contact_success')?>
<?php endif; ?></p>
 <p class="text-success">
    <?php if($this->session->flashdata('newsletter_success')): ?>
    <?php echo $this->session->flashdata('newsletter_success')?>
<?php endif; ?></p>
 <p class="text-danger">
    <?php if($this->session->flashdata('login_fail')): ?>
    <?php echo $this->session->flashdata('login_fail')?>
<?php endif; ?></p>
 <p class="text-danger">
    <?php if($this->session->flashdata('already_create')): ?>
    <?php echo $this->session->flashdata('already_create')?>
<?php endif; ?></p>
 <p class="text-success">
    <?php if($this->session->flashdata('user_registered')): ?>
    <?php echo $this->session->flashdata('user_registered')?>
<?php endif; ?></p>
<p class="text-danger">
    <?php if($this->session->flashdata('user_registered_prob')): ?>
    <?php echo $this->session->flashdata('user_registered_prob')?>
<?php endif; ?></p>
</div>

    <div id="listen" class=" mw-100 col-12 d-flex align-items-center justify-content-center ">
       
        <div id="song" class="col-3 rounded ">

           <figure>
                <figcaption>Listen our new song:</figcaption>
                 <audio id="audio" src="<?php base_url();?>assets/audio/arewealive.mp3"></audio>
               
                <i id="play_song" class="far fa-play-circle"></i>
                <i id="pause_song" class="far fa-pause-circle"></i>
              
                <p><a href="<?php base_url();?>assets/audio/arewealive.mp3" download="Are we alive">Download <span>Here</span></a></p>
            </figure>
        </div>
    </div>
    <div id="blur" class="col-12 d-flex align-items-center justify-content-center">
        <div class="row">
            <div class="col-12 justify-content-center">
            <h5 class= "text-light">UPCOMING DATE</h5>
            </div>
            <div id="date" class="col-12 border-top d-flex justify-content-between ">
                <div id="list_date"  class="text-light">
                    <ul>
                        <li><?php echo $date->city; ?>, <?php echo $date->country ?></li>
                        <li><?php echo $date->concert_hall; ?></li>
                        <li><?php echo $date->date; ?></li>
                    </ul>
                </div>
                <div class="d-flex align-items-center" >      
                    <a href="<?php echo base_url();?>home/book/<?php echo $date->id ?>">BOOK</a>
                </div>
            </div>
            <div class="col-12 justify-content-center ">
            <a href="<?php echo base_url();?>home/tour_date#blur"><p class= "text-light">View <span class="firstnamecom">ALL</span> Dates</p></a>
            </div>
        </div>
    </div>
</div>
<div class="container" >



    <p id="home_presentation"class="p-4">Now, off the heels of sold out shows and Festival appearances around the globe, ARE WE ALIVE? returns in January 2020 with their most punishing album to date: ‘Eye of the storm’. The album title, a nod to a classic Punk song by the same name, shows the bands unwillingness to conform to the typical “hardcore mold”, as well as the bitter and unforgiving lyrical content the band is known for. AWA? is a journey through a life rarely lived but often imitated. Backed by a combination of blistering metallic hardcore and west coast groove, front-man Rob Watson offers a window into a life of pain, despair, and an unrelenting will to overcome.</p>
    
    
  

</div>












