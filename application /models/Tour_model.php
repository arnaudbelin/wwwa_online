<?php  


class Tour_model extends CI_Model{

	public function get_comming_date(){

		$date = date('Y-m-d');

		$this->db->from('tour');

		$this->db->where('date >=', $date);

		$this->db->order_by("date", "desc");

		$query = $this->db->get();

		return $query->last_row();

	}

	public function get_all_dates(){

		$date = date('Y-m-d');

		$this->db->from('tour');

		// don't display past date :

		$this->db->where('date >=', $date);

		$this->db->order_by("date", "asc");

		$query = $this->db->get();

		return $query->result();

	}

	public function book_concert($id_concert){

		$this->db->where('id' , $id_concert);

		$query = $this->db->get('tour');

		return $query->row();

	}

	public function booked($id_concert, $data){

		//check if user has already booked this concert

		$booked = $this->db
		->group_start()
		->where('concert_id', $id_concert)
		->where('user_id', $this->session->userdata('user_id'))
		->group_end()
		->get('booked');

		$already_booked = $booked->row();

		if(isset($already_booked)){

			$this->session->set_flashdata('already_booked', 'Sorry, we have already booked tickets for this show.<br> Please contact us for more informations');

			redirect('home/index');

		}else{

			$insert_data = $this->db->insert('booked', $data);

		//increment booking of this concert
			
			$this->db->where('id', $id_concert);
			$book = $this->db->get('tour');
			$number_book = $book->row(5)->book;
			$this->db->from('tour');
			$this->db->where('id', $id_concert);
			$this->db->set('book', $number_book + $data['book_number']);
			$this->db->update('tour');

			return $insert_data;

		};

	}

	public function book_number($id_concert){

		$booked = $this->db
		->group_start()
		->where('concert_id', $id_concert)
		->where('user_id', $this->session->userdata('user_id'))
		->group_end()
		->get('booked');

		$already_booked = $booked->row();

		return $already_booked;

	}

	

}

?>