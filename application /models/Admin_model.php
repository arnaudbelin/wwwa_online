<?php 

class Admin_model extends CI_Model{

	public function get_date($id_concert){

		$this->db->where('id', $id_concert);

		$query = $this->db->get('tour');

		return $query->row();
	}
	public function edit_date($id_concert, $data){

		$this->db->where('id', $id_concert);

		$this->db->update('tour', $data);

		return true;
	}

	public function delete_date($id_concert){

		$this->db->where('id', $id_concert);

		$this->db->delete('tour');
	}

	public function create_date($data){

		$query = $this->db->insert('tour', $data);


		return $query;
	}
	public function get_all_message(){

		$this->db->where('done', NULL);

		$this->db->join('users', 'users.id = contact.user_id');

		$query = $this->db->get('contact');


		return $query->result();
	}
	public function get_all_email(){

		$query = $this->db->get('newsletter');

		return $query->result();
	}

	public function done_message($id_message){

		$this->db->set('done', 1);

		$this->db->where('id_message', $id_message);

		$this->db->update('contact');

		return true;

	}
	public function delete_news($id_news){

		$this->db->where('id', $id_news);

		$this->db->delete('news');
	}

	public function update_news($id_news, $data){

		$this->db->where('id', $id_news);

		$this->db->update('news', $data);

		return true;
	}

	public function get_news($id_news){

		$this->db->where('id', $id_news);

		$query = $this->db->get('news');

		return $query->row();
	}
	public function create_news($data){

		$query = $this->db->insert('news', $data);


		return $query;
	}
	public function get_all_booked_tickets(){

		$date = date('Y-m-d');
		
		$this->db->join('users', 'users.id = booked.user_id');

		$this->db->join('tour', 'tour.id = booked.concert_id');

		$this->db->where('date >=', $date);

		$this->db->order_by("date", "asc");

		$query = $this->db->get('booked');
		
		return $query->result();
	}

}