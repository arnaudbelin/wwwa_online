<?php 

class Comment_model extends CI_Model{


	public function post_comment($data){

		$insert = $this->db->insert('comment', $data);

		return $insert;


	}

	public function get_comment(){

		$this->db->order_by("date_comment", "desc");

		$this->db->limit(10);

		$query = $this->db->get('comment');

		$result = $query->result();

		return $result;
		
	}


}
