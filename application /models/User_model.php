<?php 

class User_model extends CI_Model{

	public function login_user($email, $password){

		$this->db->where('email', $email);
		
		$result = $this->db->get('users');

		$db_password = $result->row(5)->password;


		if(password_verify($password, $db_password)) {

			return $result->row();		

		}else{
			return false;
		}

	}

	public function create_user($data){

		$this->db->where('email', $data['email']);

		$create = $this->db->get('users');

		$already_create = $create->row();

		if(isset($already_create)){

			$this->session->set_flashdata('already_create', 'Sorry, this email adress is already use.');

			redirect('home/index');

		}else{

			$insert_data = $this->db->insert('users', $data);

			if($data['choose_newsletter'] == 1){

				$this->infos_model->newsletter($data['email']);
			}

			return $insert_data;

		}

	}

}







?>
